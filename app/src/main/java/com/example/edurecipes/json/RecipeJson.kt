package com.example.edurecipes.json


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RecipeJson(
    @Json(name = "count")
    val count: Int?,
    @Json(name = "from")
    val from: Int?,
    @Json(name = "hits")
    val hits: List<Hit>?,
    @Json(name = "more")
    val more: Boolean?,
    @Json(name = "q")
    val q: String?,
    @Json(name = "to")
    val to: Int?
) {
    @JsonClass(generateAdapter = true)
    data class Hit(
        @Json(name = "recipe")
        val recipe: Recipe
    ) {
        @JsonClass(generateAdapter = true)
        data class Recipe(
            @Json(name = "calories")
            val calories: Double?,
            @Json(name = "cautions")
            val cautions: List<String?>?,
            @Json(name = "cuisineType")
            val cuisineType: List<String?>?,
            @Json(name = "dietLabels")
            val dietLabels: List<String?>?,
            @Json(name = "digest")
            val digest: List<Digest?>?,
            @Json(name = "dishType")
            val dishType: List<String?>?,
            @Json(name = "healthLabels")
            val healthLabels: List<String>?,
            @Json(name = "image")
            val image: String?,
            @Json(name = "ingredientLines")
            val ingredientLines: List<String>?,
            @Json(name = "ingredients")
            val ingredients: List<Ingredient?>?,
            @Json(name = "label")
            val label: String?,
            @Json(name = "mealType")
            val mealType: List<String?>?,
            @Json(name = "shareAs")
            val shareAs: String?,
            @Json(name = "source")
            val source: String?,
            @Json(name = "totalDaily")
            val totalDaily: TotalDaily?,
            @Json(name = "totalNutrients")
            val totalNutrients: TotalNutrients?,
            @Json(name = "totalTime")
            val totalTime: Int?,
            @Json(name = "totalWeight")
            val totalWeight: Double?,
            @Json(name = "uri")
            val uri: String?,
            @Json(name = "url")
            val url: String,
            @Json(name = "yield")
            val yieldValue: Int?
        ) {
            @JsonClass(generateAdapter = true)
            data class Digest(
                @Json(name = "daily")
                val daily: Double?,
                @Json(name = "hasRDI")
                val hasRDI: Boolean?,
                @Json(name = "label")
                val label: String?,
                @Json(name = "schemaOrgTag")
                val schemaOrgTag: String?,
                @Json(name = "sub")
                val sub: List<Sub?>?,
                @Json(name = "tag")
                val tag: String?,
                @Json(name = "total")
                val total: Double?,
                @Json(name = "unit")
                val unit: String?
            ) {
                @JsonClass(generateAdapter = true)
                data class Sub(
                    @Json(name = "daily")
                    val daily: Double?,
                    @Json(name = "hasRDI")
                    val hasRDI: Boolean?,
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "schemaOrgTag")
                    val schemaOrgTag: String?,
                    @Json(name = "tag")
                    val tag: String?,
                    @Json(name = "total")
                    val total: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )
            }

            @JsonClass(generateAdapter = true)
            data class Ingredient(
                @Json(name = "foodCategory")
                val foodCategory: String?,
                @Json(name = "foodId")
                val foodId: String?,
                @Json(name = "image")
                val image: String?,
                @Json(name = "text")
                val text: String?,
                @Json(name = "weight")
                val weight: Double?
            )

            @JsonClass(generateAdapter = true)
            data class TotalDaily(
                @Json(name = "CA")
                val cA: CA?,
                @Json(name = "CHOCDF")
                val cHOCDF: CHOCDF?,
                @Json(name = "CHOLE")
                val cHOLE: CHOLE?,
                @Json(name = "ENERC_KCAL")
                val eNERCKCAL: ENERCKCAL?,
                @Json(name = "FASAT")
                val fASAT: FASAT?,
                @Json(name = "FAT")
                val fAT: FAT?,
                @Json(name = "FE")
                val fE: FE?,
                @Json(name = "FIBTG")
                val fIBTG: FIBTG?,
                @Json(name = "FOLDFE")
                val fOLDFE: FOLDFE?,
                @Json(name = "K")
                val k: K?,
                @Json(name = "MG")
                val mG: MG?,
                @Json(name = "NA")
                val nA: NA?,
                @Json(name = "NIA")
                val nIA: NIA?,
                @Json(name = "P")
                val p: P?,
                @Json(name = "PROCNT")
                val pROCNT: PROCNT?,
                @Json(name = "RIBF")
                val rIBF: RIBF?,
                @Json(name = "THIA")
                val tHIA: THIA?,
                @Json(name = "TOCPHA")
                val tOCPHA: TOCPHA?,
                @Json(name = "VITA_RAE")
                val vITARAE: VITARAE?,
                @Json(name = "VITB12")
                val vITB12: VITB12?,
                @Json(name = "VITB6A")
                val vITB6A: VITB6A?,
                @Json(name = "VITC")
                val vITC: VITC?,
                @Json(name = "VITD")
                val vITD: VITD?,
                @Json(name = "VITK1")
                val vITK1: VITK1?,
                @Json(name = "ZN")
                val zN: ZN?
            ) {
                @JsonClass(generateAdapter = true)
                data class CA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class CHOCDF(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class CHOLE(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class ENERCKCAL(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FASAT(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FAT(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FE(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FIBTG(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FOLDFE(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class K(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class MG(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class NA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class NIA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class P(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class PROCNT(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class RIBF(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class THIA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class TOCPHA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITARAE(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITB12(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITB6A(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITC(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITD(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITK1(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class ZN(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )
            }

            @JsonClass(generateAdapter = true)
            data class TotalNutrients(
                @Json(name = "CA")
                val cA: CA?,
                @Json(name = "CHOCDF")
                val cHOCDF: CHOCDF?,
                @Json(name = "CHOLE")
                val cHOLE: CHOLE?,
                @Json(name = "ENERC_KCAL")
                val eNERCKCAL: ENERCKCAL?,
                @Json(name = "FAMS")
                val fAMS: FAMS?,
                @Json(name = "FAPU")
                val fAPU: FAPU?,
                @Json(name = "FASAT")
                val fASAT: FASAT?,
                @Json(name = "FAT")
                val fAT: FAT?,
                @Json(name = "FATRN")
                val fATRN: FATRN?,
                @Json(name = "FE")
                val fE: FE?,
                @Json(name = "FIBTG")
                val fIBTG: FIBTG?,
                @Json(name = "FOLAC")
                val fOLAC: FOLAC?,
                @Json(name = "FOLDFE")
                val fOLDFE: FOLDFE?,
                @Json(name = "FOLFD")
                val fOLFD: FOLFD?,
                @Json(name = "K")
                val k: K?,
                @Json(name = "MG")
                val mG: MG?,
                @Json(name = "NA")
                val nA: NA?,
                @Json(name = "NIA")
                val nIA: NIA?,
                @Json(name = "P")
                val p: P?,
                @Json(name = "PROCNT")
                val pROCNT: PROCNT?,
                @Json(name = "RIBF")
                val rIBF: RIBF?,
                @Json(name = "SUGAR")
                val sUGAR: SUGAR?,
                @Json(name = "THIA")
                val tHIA: THIA?,
                @Json(name = "TOCPHA")
                val tOCPHA: TOCPHA?,
                @Json(name = "VITA_RAE")
                val vITARAE: VITARAE?,
                @Json(name = "VITB12")
                val vITB12: VITB12?,
                @Json(name = "VITB6A")
                val vITB6A: VITB6A?,
                @Json(name = "VITC")
                val vITC: VITC?,
                @Json(name = "VITD")
                val vITD: VITD?,
                @Json(name = "VITK1")
                val vITK1: VITK1?,
                @Json(name = "WATER")
                val wATER: WATER?,
                @Json(name = "ZN")
                val zN: ZN?
            ) {
                @JsonClass(generateAdapter = true)
                data class CA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class CHOCDF(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class CHOLE(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class ENERCKCAL(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FAMS(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FAPU(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FASAT(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FAT(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FATRN(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FE(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FIBTG(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FOLAC(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FOLDFE(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class FOLFD(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class K(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class MG(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class NA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class NIA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class P(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class PROCNT(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class RIBF(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class SUGAR(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class THIA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class TOCPHA(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITARAE(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITB12(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITB6A(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITC(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITD(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class VITK1(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class WATER(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )

                @JsonClass(generateAdapter = true)
                data class ZN(
                    @Json(name = "label")
                    val label: String?,
                    @Json(name = "quantity")
                    val quantity: Double?,
                    @Json(name = "unit")
                    val unit: String?
                )
            }
        }
    }
}