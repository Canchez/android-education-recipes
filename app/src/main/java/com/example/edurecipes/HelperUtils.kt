package com.example.edurecipes

import android.content.Context
import android.widget.Toast

class HelperUtils {
    companion object {
        /**
         * Helper function for showing toasts with [text] and [length]
         */
        fun showToast(context: Context, text: String, length: Int = Toast.LENGTH_SHORT) {
            Toast.makeText(
                context,
                text,
                length
            ).show()
        }

        /**
         * Helper function to format [ingredients]
         */
        fun formatIngredients(ingredients: List<String>): String {
            var text = ""
            ingredients.forEach { ingredient ->
                text = if (text.isEmpty()) {
                    ingredient
                } else {
                    "$text \n$ingredient"
                }
            }
            if (text.isEmpty()) {
                text = "Ingredients are empty"
            }
            return text
        }

        /**
         * Helper function to format [calories]
         */
        fun formatCalories(calories: Double) = if (calories > 0) "%.1f".format(calories) else "Undefined"

        /**
         * Helper function to format [weight]
         */
        fun formatWeight(weight: Double) = if (weight > 0) "%.2f".format(weight) else "Undefined"

        /**
         * Helper function to format [time]
         */
        fun formatTime(time: Int): String {
            return when {
                time <= 0 -> "Undefined"
                time > 60 -> "${time / 60} h ${time % 60} m"
                else -> "$time m"
            }
        }
    }
}