package com.example.edurecipes

import android.app.Application
import com.example.edurecipes.ui.notes.noteitem.room.NoteDatabase
import com.example.edurecipes.ui.notes.noteitem.room.NoteRepository
import com.yandex.mapkit.MapKitFactory

/**
 * Custom application class with values for accessing database
 */
class RecipesApplication : Application() {
    /**
     * Singleton of [NoteDatabase]
     */
    private val database by lazy { NoteDatabase.getDatabase(this) }

    /**
     * Repository of [NoteDatabase]
     */
    val noteRepository by lazy { NoteRepository(database.noteDao()) }

    override fun onCreate() {
        super.onCreate()
        MapKitFactory.setApiKey(MAP_API_KEY)
    }

    companion object {
        const val MAP_API_KEY = "d1d145c7-ff05-487d-8561-b885ad59dec2"
    }
}