package com.example.edurecipes.ui.map

import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.edurecipes.HelperUtils
import com.example.edurecipes.R
import com.google.android.material.textfield.TextInputEditText
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.location.Location
import com.yandex.mapkit.location.LocationListener
import com.yandex.mapkit.location.LocationManager
import com.yandex.mapkit.location.LocationStatus
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.VisibleRegionUtils
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.search.*
import com.yandex.runtime.Error
import com.yandex.runtime.image.ImageProvider

/**
 * Fragment with map
 */
class MapFragment : Fragment(), Session.SearchListener {

    private lateinit var mapView: MapView
    private lateinit var btnCurrentLocation: Button
    private var isLocationEnabled: Boolean = false
    private lateinit var locationManager: LocationManager
    private lateinit var searchManager: SearchManager
    private lateinit var searchSession: Session

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_map, container, false)

        val teSearchBar: TextInputEditText = root.findViewById(R.id.teSearchBar)
        teSearchBar.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchOnMap(view.text.toString())
                true
            } else {
                false
            }
        }

        btnCurrentLocation = root.findViewById(R.id.btnCurrentLocation)
        btnCurrentLocation.setOnClickListener {
            if (isLocationEnabled) {
                goToCurrentLocation()
            }
        }

        MapKitFactory.initialize(requireContext())
        SearchFactory.initialize(requireContext())

        mapView = root.findViewById(R.id.mapView)
        locationManager = MapKitFactory.getInstance().createLocationManager()
        searchManager = SearchFactory.getInstance().createSearchManager(SearchManagerType.COMBINED)

        checkPermission()

        if (isLocationEnabled) goToCurrentLocation()

        return root
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
        MapKitFactory.getInstance().onStop()
    }

    override fun onSearchResponse(response: Response) {
        val mapObjects = mapView.map.mapObjects
        mapObjects.clear()

        for (searchResult in response.collection.children) {
            val resultLocation = searchResult.obj?.geometry?.get(0)?.point
            if (resultLocation != null) {
                mapObjects.addPlacemark(
                    resultLocation,
                    ImageProvider.fromResource(requireContext(), R.drawable.ic_search_result)
                )
            }
        }
    }

    override fun onSearchError(error: Error) {
        Log.d(TAG, error.toString())
        HelperUtils.showToast(requireContext(), "Search Error occurred")
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        isLocationEnabled = if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                btnCurrentLocation.isEnabled = true
                true
            } else {
                btnCurrentLocation.isEnabled = false
                HelperUtils.showToast(requireContext(), "Map location is not available because permission not granted")
                false
            }
        } else {
            return
        }
    }

    /**
     * Go to current device location
     */
    private fun goToCurrentLocation() {
        locationManager.requestSingleUpdate(
            object : LocationListener {
                override fun onLocationStatusUpdated(status: LocationStatus) {
                }

                override fun onLocationUpdated(location: Location) {
                    Log.d(TAG, "long=${location.position.longitude}, lat=${location.position.latitude}")
                    mapView.map.move(
                        CameraPosition(location.position, 16.0f, 0.0f, 0.0f),
                        Animation(Animation.Type.SMOOTH, 1.0f),
                        null
                    )
                }
            }
        )
    }

    /**
     * Check permission for location
     */
    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            isLocationEnabled = true
        } else {
            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
        }
    }

    /**
     * Handle search [query] for map
     */
    private fun searchOnMap(query: String) {
        searchSession = searchManager.submit(
            query,
            VisibleRegionUtils.toPolygon(mapView.map.visibleRegion),
            SearchOptions(),
            this
        )
    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val TAG = "MapFragment"
    }
}