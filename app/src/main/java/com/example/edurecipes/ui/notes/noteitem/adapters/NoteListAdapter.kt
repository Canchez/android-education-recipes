package com.example.edurecipes.ui.notes.noteitem.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.edurecipes.R
import com.example.edurecipes.ui.notes.noteitem.room.NoteEntity

/**
 * Adapter for notes
 */
class NoteListAdapter(
    private val inflater: LayoutInflater,
    private val onNoteListener: OnNoteListener,
) : ListAdapter<NoteEntity, NoteViewHolder>(NoteDiffer) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val view = inflater.inflate(R.layout.item_note, parent, false)
        return NoteViewHolder(view, onNoteListener)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    private object NoteDiffer : DiffUtil.ItemCallback<NoteEntity>() {
        override fun areItemsTheSame(oldItem: NoteEntity, newItem: NoteEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NoteEntity, newItem: NoteEntity): Boolean {
            return oldItem == newItem
        }

    }

}