package com.example.edurecipes.ui.notes.noteitem.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

/**
 * DAO for notes
 */
@Dao
interface NoteDao {
    /**
     * Get all notes from database
     */
    @Query("SELECT * FROM notes ORDER BY id ASC")
    fun getAllNotes(): Flow<List<NoteEntity>>

    /**
     * Insert new note
     */
    @Insert
    suspend fun insertNote(note: NoteEntity)

    /**
     * Delete existing note by id
     */
    @Query("DELETE FROM notes WHERE id = :id")
    suspend fun deleteNoteById(id: Long)

    /**
     * Update existing note
     */
    @Update
    suspend fun updateNote(note: NoteEntity)
}