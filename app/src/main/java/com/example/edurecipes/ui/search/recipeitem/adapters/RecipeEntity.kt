package com.example.edurecipes.ui.search.recipeitem.adapters

/**
 * Entity for recipe
 *
 * @property title Title of recipe
 * @property imageUrl URL of image for recipe
 * @property url URL of recipe
 * @property calories Amount of calories
 * @property totalWeight Weight of food
 * @property time Time to cook
 * @property ingredients List of ingredients
 */
data class RecipeEntity(
    val title: String = "",
    val imageUrl: String = "",
    val url: String = "",
    val calories: Double = 0.0,
    val totalWeight: Double = 0.0,
    val time: Int = 0,
    val ingredients: List<String> = listOf(),
)