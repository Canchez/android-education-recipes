package com.example.edurecipes.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.edurecipes.ui.search.recipeitem.adapters.RecipeEntity

/**
 * ViewModel for search items
 */
class SearchViewModel : ViewModel() {
    /**
     * Items of search
     */
    var searchItems = MutableLiveData<List<RecipeEntity>>()

    /**
     * Set [searchItems] to [items]
     */
    fun setItems(items: List<RecipeEntity>) {
        searchItems.value = items
    }

    /**
     * Get item from [searchItems] at [position] or null
     */
    fun getItem(position: Int) = searchItems.value?.get(position)
}

/**
 * Factory for [SearchViewModel]
 */
class SearchViewModelFactory() : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SearchViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}