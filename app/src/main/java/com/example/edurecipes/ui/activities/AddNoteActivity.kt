package com.example.edurecipes.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import com.example.edurecipes.R
import com.example.edurecipes.ui.notes.NotesFragment
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

/**
 * Activity for creating new note
 */
class AddNoteActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)

        val tilNoteTitle: TextInputLayout = findViewById(R.id.tilEditNoteTitle)
        val tvNewTitle: TextInputEditText = findViewById(R.id.teEditNoteTitle)
        val tvText: TextView = findViewById(R.id.tvNewNoteDesc)

        tvNewTitle.doOnTextChanged { _, _, _, _ -> tilNoteTitle.error = "" }
        tvText.doOnTextChanged { _, _, _, _ -> tilNoteTitle.error = "" }

        val btnAddNewNote: Button = findViewById(R.id.btnAddNewNote)
        btnAddNewNote.setOnClickListener {
            val newTitle = tvNewTitle.text.toString()
            val newText = tvText.text.toString()
            if (newTitle.isEmpty() || newText.isEmpty()) {
                tilNoteTitle.error = getString(R.string.empty_error)
            } else {
                val returnIntent = Intent()
                returnIntent.putExtra(NotesFragment.NOTE_TITLE, newTitle)
                returnIntent.putExtra(NotesFragment.NOTE_TEXT, newText)
                setResult(RESULT_OK, returnIntent)
                finish()
            }
        }
    }
}