package com.example.edurecipes.ui.search.recipeitem.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.edurecipes.R

/**
 * Adapter for recipes
 */
class RecipeListAdapter(
    private val inflater: LayoutInflater,
    private val onSearchItemListener: OnSearchItemListener,
) : ListAdapter<RecipeEntity, RecipeViewHolder>(RecipeDiffer) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val view = inflater.inflate(R.layout.item_search_node, parent, false)
        return RecipeViewHolder(view, onSearchItemListener)
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    private object RecipeDiffer : DiffUtil.ItemCallback<RecipeEntity>() {
        override fun areItemsTheSame(oldItem: RecipeEntity, newItem: RecipeEntity): Boolean {
            return oldItem.title == oldItem.title
        }

        override fun areContentsTheSame(oldItem: RecipeEntity, newItem: RecipeEntity): Boolean {
            return oldItem == newItem
        }
    }
}