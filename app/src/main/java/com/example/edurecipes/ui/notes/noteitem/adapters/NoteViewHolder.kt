package com.example.edurecipes.ui.notes.noteitem.adapters

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.edurecipes.R
import com.example.edurecipes.ui.notes.noteitem.room.NoteEntity

/**
 * ViewHolder for note
 */
class NoteViewHolder(
    private val view: View,
    private val onNoteListener: OnNoteListener,
) : RecyclerView.ViewHolder(view), View.OnClickListener {
    fun bindTo(noteEntity: NoteEntity) {
        val tvTitle: TextView = view.findViewById(R.id.tvNoteTitle)
        val tvText: TextView = view.findViewById(R.id.tvNoteText)
        tvTitle.text = noteEntity.title
        tvText.text = noteEntity.text

        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        onNoteListener.onNoteClick(adapterPosition)
    }
}