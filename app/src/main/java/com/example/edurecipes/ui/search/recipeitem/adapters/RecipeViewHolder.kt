package com.example.edurecipes.ui.search.recipeitem.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.edurecipes.HelperUtils
import com.example.edurecipes.R

/**
 * ViewHolder for recipe
 */
class RecipeViewHolder(
    private val view: View,
    private val onSearchItemListener: OnSearchItemListener,
) :
    RecyclerView.ViewHolder(view), View.OnClickListener {
    fun bindTo(recipeEntity: RecipeEntity) {
        val ivImage: ImageView = view.findViewById(R.id.ivRecipeImage)
        val tvTitle: TextView = view.findViewById(R.id.tvRecipeTitle)
        val tvCalories: TextView = view.findViewById(R.id.tvCalories)
        val tvTime: TextView = view.findViewById(R.id.tvTime)
        val tvWeight: TextView = view.findViewById(R.id.tvWeight)
        val tvHealth: TextView = view.findViewById(R.id.tvHealth)

        tvTitle.text = recipeEntity.title
        tvCalories.text = HelperUtils.formatCalories(recipeEntity.calories)
        tvTime.text = HelperUtils.formatTime(recipeEntity.time)
        tvWeight.text = HelperUtils.formatWeight(recipeEntity.totalWeight)
        tvHealth.text = HelperUtils.formatIngredients(recipeEntity.ingredients)

        Glide.with(view)
            .load(recipeEntity.imageUrl)
            .error(R.drawable.ic_launcher_background)
            .placeholder(R.drawable.ic_placeholder_200dp)
            .into(ivImage)

        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        onSearchItemListener.onSearchItemClick(adapterPosition)
    }
}