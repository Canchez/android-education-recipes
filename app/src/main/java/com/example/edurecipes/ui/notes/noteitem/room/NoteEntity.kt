package com.example.edurecipes.ui.notes.noteitem.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity for note
 *
 * @property id Autogenerated id of note
 * @property title Title of note
 * @property text Text of note
 */
@Entity(tableName = "notes")
data class NoteEntity(
    @ColumnInfo(name = "note_title")
    var title: String = "",
    @ColumnInfo(name = "note_text")
    var text: String = ""
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0
}