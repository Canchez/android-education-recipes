package com.example.edurecipes.ui.notes.noteitem.room

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

/**
 * Repository for notes
 */
class NoteRepository(private val noteDao: NoteDao) {

    /**
     * FLow of all notes
     */
    val allNotes: Flow<List<NoteEntity>> = noteDao.getAllNotes()

    /**
     * Insert new note
     */
    @WorkerThread
    suspend fun insert(note: NoteEntity) = noteDao.insertNote(note)

    /**
     * Update existing note
     */
    @WorkerThread
    suspend fun update(note: NoteEntity) = noteDao.updateNote(note)

    /**
     * Delete existing note by id
     */
    @WorkerThread
    suspend fun delete(id: Long) = noteDao.deleteNoteById(id)
}