package com.example.edurecipes.ui.search.recipeitem.adapters

/**
 * Interface to implement clicks on search item
 */
interface OnSearchItemListener {
    fun onSearchItemClick(position: Int)
}