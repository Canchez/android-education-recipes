package com.example.edurecipes.ui.notes

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.edurecipes.HelperUtils
import com.example.edurecipes.R
import com.example.edurecipes.RecipesApplication
import com.example.edurecipes.ui.activities.AddNoteActivity
import com.example.edurecipes.ui.activities.EditNoteActivity
import com.example.edurecipes.ui.notes.noteitem.adapters.NoteListAdapter
import com.example.edurecipes.ui.notes.noteitem.adapters.OnNoteListener
import com.example.edurecipes.ui.notes.noteitem.room.NoteEntity

/**
 * Fragment for note list
 */
class NotesFragment : Fragment(), OnNoteListener {

    private val noteViewModel: NoteViewModel by viewModels {
        NoteViewModelFactory((requireActivity().application as RecipesApplication).noteRepository)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_notes, container, false)

        val rvNotes: RecyclerView = root.findViewById(R.id.rvNoteList)
        rvNotes.layoutManager = LinearLayoutManager(context)
        val notesAdapter = NoteListAdapter(layoutInflater, this)
        rvNotes.adapter = notesAdapter

        val btnAddNote: Button = root.findViewById(R.id.btnAddNote)
        btnAddNote.setOnClickListener { startAddNoteActivity() }

        noteViewModel.allNotes.observe(viewLifecycleOwner) { notes ->
            notes?.let { notesAdapter.submitList(it) }
        }

        return root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ADD_NOTE_ACTIVITY_REQUEST_CODE -> {
                if (resultCode == RESULT_OK && data != null) {
                    handleAddNote(data)
                }
            }
            EDIT_NOTE_ACTIVITY_REQUEST_CODE -> {
                when (resultCode) {
                    RESULT_EDIT -> {
                        handleEditNote(data)
                    }
                    RESULT_DELETE -> {
                        handleDeleteNote(data)
                    }
                    RESULT_ERROR -> {
                        HelperUtils.showToast(requireContext(), "Error with editing note")
                    }
                }
            }
        }
    }

    override fun onNoteClick(position: Int) {
        val clickedNote = noteViewModel.allNotes.value?.get(position)
        if (clickedNote != null) {
            startEditNoteActivity(clickedNote)
        }
    }

    /**
     * Handle adding note after [AddNoteActivity] is finished with [RESULT_OK]
     */
    private fun handleAddNote(data: Intent) {
        val title = data.getStringExtra(NOTE_TITLE)
        val text = data.getStringExtra(NOTE_TEXT)
        if (title != null && text != null) {
            val note = NoteEntity(title, text)
            noteViewModel.insert(note)
            HelperUtils.showToast(requireContext(), "Saved note with title ($title)")
        }
    }

    /**
     * Handle deleting note after [EditNoteActivity] is finished with [RESULT_DELETE]
     */
    private fun handleDeleteNote(data: Intent?) {
        if (data != null) {
            val curId = data.getLongExtra(NOTE_ID, -1L)
            if (curId > 0L) {
                noteViewModel.delete(curId)
                HelperUtils.showToast(requireContext(), "Deleted note, id=$curId")
            }
        }
    }

    /**
     * Handle editing note after [EditNoteActivity] is finished with [RESULT_EDIT]
     */
    private fun handleEditNote(data: Intent?) {
        if (data != null) {
            val curId = data.getLongExtra(NOTE_ID, -1L)
            val newTitle = data.getStringExtra(NOTE_TITLE)
            val newText = data.getStringExtra(NOTE_TEXT)
            if (newTitle != null && newText != null && curId > 0L) {
                val newNote = NoteEntity(newTitle, newText)
                newNote.id = curId
                noteViewModel.update(newNote)
                HelperUtils.showToast(requireContext(), "Updated note with title ($newTitle), id=$curId")
            }
        }
    }


    /**
     * Start new [AddNoteActivity]
     */
    private fun startAddNoteActivity() {
        val intent = Intent(context, AddNoteActivity::class.java)
        startActivityForResult(intent, ADD_NOTE_ACTIVITY_REQUEST_CODE)
    }

    /**
     * Start new [EditNoteActivity] with info from [note]
     */
    private fun startEditNoteActivity(note: NoteEntity) {
        val intent = Intent(context, EditNoteActivity::class.java)
        intent.putExtra(NOTE_ID, note.id)
        intent.putExtra(NOTE_TITLE, note.title)
        intent.putExtra(NOTE_TEXT, note.text)
        startActivityForResult(intent, EDIT_NOTE_ACTIVITY_REQUEST_CODE)
    }


    companion object {
        const val ADD_NOTE_ACTIVITY_REQUEST_CODE = 1
        const val EDIT_NOTE_ACTIVITY_REQUEST_CODE = 2

        const val NOTE_ID = "noteId"
        const val NOTE_TITLE = "noteTitle"
        const val NOTE_TEXT = "noteText"

        const val RESULT_ERROR = 5
        const val RESULT_DELETE = 6
        const val RESULT_EDIT = 7
    }
}