package com.example.edurecipes.ui.notes.noteitem.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

private const val DB_NAME = "notes.db"

/**
 * Database for notes
 */
@Database(entities = [NoteEntity::class], version = 1)
abstract class NoteDatabase : RoomDatabase() {
    abstract fun noteDao(): NoteDao

    companion object {
        @Volatile
        private var INSTANCE: NoteDatabase? = null

        /**
         * Get singleton for database
         */
        @Synchronized
        fun getDatabase(context: Context): NoteDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, NoteDatabase::class.java, DB_NAME).build()
            }
            return INSTANCE!!
        }
    }
}