package com.example.edurecipes.ui.notes.noteitem.adapters

/**
 * Interface to implement clicks on note
 */
interface OnNoteListener {
    fun onNoteClick(position: Int)
}