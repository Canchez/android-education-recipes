package com.example.edurecipes.ui.search

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.edurecipes.HelperUtils
import com.example.edurecipes.R
import com.example.edurecipes.json.RecipeJson
import com.example.edurecipes.ui.search.recipeitem.adapters.OnSearchItemListener
import com.example.edurecipes.ui.search.recipeitem.adapters.RecipeEntity
import com.example.edurecipes.ui.search.recipeitem.adapters.RecipeListAdapter
import com.google.android.material.textfield.TextInputEditText
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.json.JSONObject

/**
 * Fragment for searching recipes from API
 */
class SearchFragment : Fragment(), OnSearchItemListener {

    private lateinit var tvEmpty: TextView
    private lateinit var rvRecipes: RecyclerView
    private lateinit var tvLoading: TextView

    private val searchViewModel: SearchViewModel by viewModels {
        SearchViewModelFactory()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_search, container, false)

        tvEmpty = root.findViewById(R.id.tvEmptyList)
        tvLoading = root.findViewById(R.id.tvLoading)
        rvRecipes = root.findViewById(R.id.rvSearchItems)
        changeUiVisibility(isEmptyVisible = true, isLoadingVisible = false)

        rvRecipes.layoutManager = LinearLayoutManager(context)
        val recipesAdapter = RecipeListAdapter(layoutInflater, this)
        rvRecipes.adapter = recipesAdapter

        val teSearchBar: TextInputEditText = root.findViewById(R.id.teSearchBar)

        teSearchBar.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val text = view.text.toString()
                if (text.isNotEmpty()) {
                    changeUiVisibility(isEmptyVisible = false, isLoadingVisible = true)
                    sendRequest(text)
                }
                true
            } else {
                false
            }
        }

        searchViewModel.searchItems.observe(viewLifecycleOwner) { items ->
            items?.let { recipesAdapter.submitList(it) }
        }

        return root
    }

    override fun onSearchItemClick(position: Int) {
        val clickedSearchItem = searchViewModel.getItem(position)
        if (clickedSearchItem != null) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(clickedSearchItem.url))
            startActivity(intent)
        }
    }


    /**
     * Send request with [query] from [fromIndex] to [toIndex]
     */
    private fun sendRequest(query: String, fromIndex: Int = 0, toIndex: Int = 10) {
        val queue = Volley.newRequestQueue(context)

        val url = "https://api.edamam.com/search?q=$query&app_id=$API_APP_ID&app_key=$API_APP_KEY&from=$fromIndex&to=$toIndex"

        val customRequest = JsonObjectRequest(Request.Method.GET, url, null,
            { response -> handleResponse(response) },
            {
                changeUiVisibility(isEmptyVisible = true, isLoadingVisible = false)
                Log.d(TAG, it.toString())
            })

        queue.add(customRequest)
    }

    /**
     * Handle [response] and populate search list if response is correct
     */
    private fun handleResponse(response: JSONObject) {
        Log.d(TAG, response.toString())

        val jsonAdapter: JsonAdapter<RecipeJson> = Moshi.Builder().build().adapter(RecipeJson::class.java)
        val responseObject = jsonAdapter.fromJson(response.toString())

        val recipeEntityList = mutableListOf<RecipeEntity>()
        if (responseObject != null) {
            if (!responseObject.hits.isNullOrEmpty()) {
                responseObject.hits.forEach { hit ->
                    val recipeEntity = hit.recipe.let { recipe ->
                        RecipeEntity(
                            title = recipe.label ?: "error",
                            imageUrl = recipe.image ?: "",
                            url = recipe.url,
                            calories = recipe.calories ?: -1.0,
                            totalWeight = recipe.totalWeight ?: -1.0,
                            time = recipe.totalTime ?: -1,
                            ingredients = recipe.ingredientLines ?: listOf()
                        )
                    }
                    recipeEntityList.add(recipeEntity)
                }
                changeUiVisibility(isEmptyVisible = false, isLoadingVisible = false)
                searchViewModel.setItems(recipeEntityList)
            } else {
                changeUiVisibility(isEmptyVisible = true, isLoadingVisible = false)
            }
        } else {
            changeUiVisibility(isEmptyVisible = true, isLoadingVisible = false)
            HelperUtils.showToast(requireContext(), "Error with response")
        }
    }

    /**
     * Change visibility of [tvEmpty], [tvLoading] and [rvRecipes] depending on state
     */
    private fun changeUiVisibility(isEmptyVisible: Boolean, isLoadingVisible: Boolean) {
        tvEmpty.isVisible = isEmptyVisible
        tvLoading.isVisible = isLoadingVisible
        rvRecipes.isVisible = !(isEmptyVisible || isLoadingVisible)
    }

    companion object {
        const val API_APP_ID = "9a8aa758"
        const val API_APP_KEY = "9f34f86b41d2b084d8a0cdb3f2daef43"
        const val TAG = "SearchFragment"
    }
}