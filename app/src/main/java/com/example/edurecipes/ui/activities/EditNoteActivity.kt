package com.example.edurecipes.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import com.example.edurecipes.R
import com.example.edurecipes.ui.notes.NotesFragment
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

/**
 * Activity for editing or deleting existing note
 */
class EditNoteActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_note)

        val tilNoteTitle: TextInputLayout = findViewById(R.id.tilEditNoteTitle)
        val tvTitle: TextInputEditText = findViewById(R.id.teEditNoteTitle)
        val tvText: TextView = findViewById(R.id.tvEditNoteDesc)


        var curId = -1L
        val extras = intent.extras
        if (extras != null) {
            curId = extras.getLong(NotesFragment.NOTE_ID, -1L)
            val curTitle = extras.getString(NotesFragment.NOTE_TITLE)
            val curText = extras.getString(NotesFragment.NOTE_TEXT)
            if (curId > 0 && curTitle != null && curText != null) {
                tvTitle.setText(curTitle)
                tvText.text = curText
            } else {
                setResult(NotesFragment.RESULT_ERROR)
                finish()
            }
        }

        tvTitle.doOnTextChanged { _, _, _, _ -> tilNoteTitle.error = "" }
        tvText.doOnTextChanged { _, _, _, _ -> tilNoteTitle.error = "" }

        val btnSaveNote: Button = findViewById(R.id.btnSaveNote)
        val btnDeleteNote: Button = findViewById(R.id.btnDeleteNote)

        btnSaveNote.setOnClickListener {
            val newTitle = tvTitle.text.toString()
            val newText = tvText.text.toString()
            if (newTitle.isEmpty() || newText.isEmpty()) {
                tilNoteTitle.error = getString(R.string.empty_error)
            } else {
                val returnIntent = Intent()
                returnIntent.putExtra(NotesFragment.NOTE_ID, curId)
                returnIntent.putExtra(NotesFragment.NOTE_TITLE, newTitle)
                returnIntent.putExtra(NotesFragment.NOTE_TEXT, newText)
                setResult(NotesFragment.RESULT_EDIT, returnIntent)
                finish()
            }
        }

        btnDeleteNote.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra(NotesFragment.NOTE_ID, curId)
            setResult(NotesFragment.RESULT_DELETE, returnIntent)
            finish()
        }

    }
}