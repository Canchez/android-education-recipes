package com.example.edurecipes.ui.notes

import androidx.lifecycle.*
import com.example.edurecipes.ui.notes.noteitem.room.NoteEntity
import com.example.edurecipes.ui.notes.noteitem.room.NoteRepository
import kotlinx.coroutines.launch

/**
 * ViewModel for notes
 */
class NoteViewModel(private val repository: NoteRepository) : ViewModel() {

    /**
     * All notes from database
     */
    val allNotes: LiveData<List<NoteEntity>> = repository.allNotes.asLiveData()

    /**
     * Add [note] to database
     */
    fun insert(note: NoteEntity) = viewModelScope.launch {
        repository.insert(note)
    }

    /**
     * Update [note]
     */
    fun update(note: NoteEntity) = viewModelScope.launch {
        repository.update(note)
    }

    /**
     * Delete note by [id]
     */
    fun delete(id: Long) = viewModelScope.launch {
        repository.delete(id)
    }
}

/**
 * Factory for [NoteViewModel]
 */
class NoteViewModelFactory(private val repository: NoteRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NoteViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return NoteViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}